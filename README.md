# smart-ioc

#### 项目介绍
ioc：IO for Client

运用Java NIO技术实现的Client版smart-socket，又名smart-ioc。

smart-ioc只提供Client端的通信服务，如需使用Server服务请移步[smart-socket](https://gitee.com/smartboot/smart-socket)。

smart-socket已包含全套Server/Client功能，为何还开发smart-ioc?因为我们希望从事Android开发的朋友也能体验到smart-socket的魅力。

官方唯一指定QQ交流群：172299083