package org.smartboot.ioc.protocol.p2p.client;

import org.smartboot.ioc.protocol.p2p.P2PProtocol;
import org.smartboot.ioc.protocol.p2p.message.BaseMessage;
import org.smartboot.ioc.protocol.p2p.message.DetectMessageReq;
import org.smartboot.ioc.protocol.p2p.message.DetectMessageResp;
import org.smartboot.ioc.protocol.p2p.message.P2pServiceMessageFactory;
import org.smartboot.ioc.transport.NioQuickClient;

import java.util.Properties;

public class P2PClient {
    public static void main(String[] args) throws Exception {
        Properties properties = new Properties();
        properties.put(DetectMessageResp.class.getName(), "");
        P2pServiceMessageFactory messageFactory = new P2pServiceMessageFactory();
        try {
            messageFactory.loadFromProperties(properties);
        } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        final P2PClientMessageProcessor processor = new P2PClientMessageProcessor();

        final NioQuickClient<BaseMessage> client = new NioQuickClient<BaseMessage>();
        client.connect("localhost", 8888)
                .setProcessor(processor)
                .setProtocol(new P2PProtocol(messageFactory))
                .start();


        long num = 0;
        while (num++ < Long.MAX_VALUE) {
            System.out.println(1);
            DetectMessageReq request = new DetectMessageReq();
            request.setDetect("台州人在杭州:" + num);
            try {
//							DetectMessageResp loginResp = (DetectMessageResp) processor.getSession()
//								.sendWithResponse(request);
                System.out.println(processor.getSession().sendWithResponse(request));
//							 Thread.sleep(1);
                // logger.info(loginResp);
            } catch (Exception e) {
                System.out.println(num);
                e.printStackTrace();
//                System.exit(0);
            }
        }
        client.shutdown();

    }
}
