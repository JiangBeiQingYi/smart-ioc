package org.smartboot.ioc.protocol.p2p.client;

import org.smartboot.ioc.MessageProcessor;
import org.smartboot.ioc.StateMachineEnum;
import org.smartboot.ioc.protocol.p2p.P2PSession;
import org.smartboot.ioc.protocol.p2p.message.BaseMessage;
import org.smartboot.ioc.transport.NioSession;

public class P2PClientMessageProcessor implements MessageProcessor<BaseMessage> {

    private P2PSession session;

    @Override
    public void process(NioSession<BaseMessage> session, BaseMessage msg) {
        System.out.println(msg);
        if(this.session.notifySyncMessage(msg)){
            return;
        }
        System.out.println(msg);
    }

    @Override
    public void stateEvent(NioSession<BaseMessage> session, StateMachineEnum stateMachineEnum, Throwable throwable) {
        switch (stateMachineEnum) {
            case NEW_SESSION:
                this.session = new P2PSession(session, 0);
                break;
        }
    }

    public P2PSession getSession() {
        return session;
    }
}
