package org.smartboot.ioc.protocol;

/**
 * 消息解码异常
 * 
 * @author三刀
 * 
 */
public class DecoderException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DecoderException(String string) {
		super(string);
	}

	public DecoderException(Throwable cause) {
		super(cause);
	}

}
